use abstract_factory::monsters::Monster;
use super::Beast;

pub struct FriendlySalamander;
pub struct NeutralSalamander;
pub struct AggresiveSalamander;

impl Monster for FriendlySalamander {

    fn say_hello(&self) {
        println!("Hi I'm a friendly salamander");
    }
}

impl Monster for NeutralSalamander {

    fn say_hello(&self) {
        println!("I'm a Neutral salamander");
    }
}

impl Monster for AggresiveSalamander {

    fn say_hello(&self) {
        println!("I'm a salamander. I kill you");
    }
}

impl Beast for FriendlySalamander{
    
}
impl Beast for NeutralSalamander{
    
}
impl Beast for AggresiveSalamander{
    
}
