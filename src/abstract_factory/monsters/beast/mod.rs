pub use self::salamander::FriendlySalamander;
pub use self::salamander::NeutralSalamander;
pub use self::salamander::AggresiveSalamander;
use super::Monster;

pub mod salamander;

pub trait Beast: Monster {

}
