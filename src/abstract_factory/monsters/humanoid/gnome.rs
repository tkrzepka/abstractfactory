use abstract_factory::monsters::Monster;
use super::Humanoid;

pub struct FriendlyGnome;
pub struct NeutralGnome;
pub struct AggresiveGnome;

impl Monster for FriendlyGnome {

    fn say_hello(&self) {
        println!("Hi I'm a friendly gnome");
    }
}

impl Monster for NeutralGnome {

    fn say_hello(&self) {
        println!("I'm a Neutral gnome");
    }
}

impl Monster for AggresiveGnome {

    fn say_hello(&self) {
        println!("I'm a gnome. I kill you");
    }
}

impl Humanoid for FriendlyGnome{
    
}
impl Humanoid for NeutralGnome{
    
}
impl Humanoid for AggresiveGnome{
    
}