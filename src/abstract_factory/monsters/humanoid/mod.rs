pub use self::gnome::FriendlyGnome;
pub use self::gnome::NeutralGnome;
pub use self::gnome::AggresiveGnome;
pub use self::leprechaun::FriendlyLeprechaun;
pub use self::leprechaun::NeutralLeprechaun;
pub use self::leprechaun::AggresiveLeprechaun;
use super::Monster;

pub mod gnome;
pub mod leprechaun;

pub trait Humanoid: Monster {

}
