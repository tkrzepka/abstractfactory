use abstract_factory::monsters::Monster;
use super::Humanoid;

pub struct FriendlyLeprechaun;
pub struct NeutralLeprechaun;
pub struct AggresiveLeprechaun;

impl Monster for FriendlyLeprechaun {

    fn say_hello(&self) {
        println!("Hi I'm a friendly leprechaun");
    }
}

impl Monster for NeutralLeprechaun {

    fn say_hello(&self) {
        println!("I'm a Neutral leprechaun");
    }
}

impl Monster for AggresiveLeprechaun {

    fn say_hello(&self) {
        println!("I'm a leprechaun. I kill you");
    }
}

impl Humanoid for FriendlyLeprechaun{
    
}
impl Humanoid for NeutralLeprechaun{
    
}
impl Humanoid for AggresiveLeprechaun{
    
}
