pub use self::humanoid::FriendlyGnome;
pub use self::humanoid::NeutralGnome;
pub use self::humanoid::AggresiveGnome;
pub use self::humanoid::FriendlyLeprechaun;
pub use self::humanoid::NeutralLeprechaun;
pub use self::humanoid::AggresiveLeprechaun;
pub use self::beast::FriendlySalamander;
pub use self::beast::NeutralSalamander;
pub use self::beast::AggresiveSalamander;

pub use self::beast::Beast;
pub use self::humanoid::Humanoid;

pub mod beast;
pub mod humanoid;

pub trait Monster {
    fn say_hello(&self);
}
