use abstract_factory::monsters::*;
use super::AbstractMonsterFactory;

pub struct AggresiveMonsterFactory;

impl AbstractMonsterFactory for AggresiveMonsterFactory {

    fn create_gnome(&self) -> Box<Humanoid> {
        box AggresiveGnome
    }

    fn create_leprechaun(&self) -> Box<Humanoid> {
        box AggresiveLeprechaun
    }

    fn create_salamander(&self) -> Box<Beast> {
        box AggresiveSalamander
    }
}
