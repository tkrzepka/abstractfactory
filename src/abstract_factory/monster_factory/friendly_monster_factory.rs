use abstract_factory::monsters::*;
use super::AbstractMonsterFactory;

pub struct FriendlyMonsterFactory;

impl AbstractMonsterFactory for FriendlyMonsterFactory {

    fn create_gnome(&self) -> Box<Humanoid> {
        box FriendlyGnome
    }

    fn create_leprechaun(&self) -> Box<Humanoid> {
        box FriendlyLeprechaun
    }

    fn create_salamander(&self) -> Box<Beast> {
        box FriendlySalamander
    }
}

#[test]
fn create_gnome() {
    let amf = FriendlyMonsterFactory;
    let gnome:Box<Humanoid> = amf.create_gnome();

    gnome.say_hello();
}