use abstract_factory::monsters::*;
use super::AbstractMonsterFactory;

pub struct NeutralMonsterFactory;

impl AbstractMonsterFactory for NeutralMonsterFactory {

    fn create_gnome(&self) -> Box<Humanoid> {
        box NeutralGnome
    }

    fn create_leprechaun(&self) -> Box<Humanoid> {
        box NeutralLeprechaun
    }

    fn create_salamander(&self) -> Box<Beast> {
        box NeutralSalamander
    }
}
