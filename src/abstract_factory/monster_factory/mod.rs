use abstract_factory::monsters::beast::Beast;
use abstract_factory::monsters::humanoid::Humanoid;

pub use self::friendly_monster_factory::FriendlyMonsterFactory;
pub use self::neutral_monster_factory::NeutralMonsterFactory;
pub use self::aggresive_monster_factory::AggresiveMonsterFactory;

pub mod friendly_monster_factory;
pub mod neutral_monster_factory;
pub mod aggresive_monster_factory;

pub trait AbstractMonsterFactory {

    fn create_gnome(&self) -> Box<Humanoid>;
    fn create_leprechaun(&self) -> Box<Humanoid>;
    fn create_salamander(&self) -> Box<Beast>;
}
