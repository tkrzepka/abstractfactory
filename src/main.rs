#![feature(box_syntax)]
extern crate abstract_factory;

use abstract_factory::abstract_factory::*;

fn main() {

    let mut amf: Box<AbstractMonsterFactory> = box AggresiveMonsterFactory;
    let mut gnome:Box<Humanoid> = amf.create_gnome();
    let mut salam:Box<Beast> = amf.create_salamander();
    let mut lepre:Box<Humanoid> = amf.create_leprechaun();
    gnome.say_hello();
    salam.say_hello();
    lepre.say_hello();

    amf = box FriendlyMonsterFactory;
    gnome = amf.create_gnome();
    salam = amf.create_salamander();
    lepre = amf.create_leprechaun();
    gnome.say_hello();
    salam.say_hello();
    lepre.say_hello();

    amf = box NeutralMonsterFactory;
    gnome = amf.create_gnome();
    salam = amf.create_salamander();
    lepre = amf.create_leprechaun();
    gnome.say_hello();
    salam.say_hello();
    lepre.say_hello();
}